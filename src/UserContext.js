import React from 'react';

//Create a context object
//bale ung ginagawa nito, is nag ce-create lang ng context objects

const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;

//context object
// UserContext {
//      user: ""
//      setUser: () => {}
//      unsetUser: () => {}
//      UserProvider: () => {}
//  
// }

