const coursesData = [
    {
      id: "wdc001",
      name: "PHP - Laravel",
      description:
        "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
      price: 45000,
      onOffer: true,
      start_date: "2021-02-20",
      end_date: "2021-08-19"
    },
    {
      id: "wdc002",
      name: "Python - Django",
      description:
        "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
      price: 30000,
      onOffer: true,
      start_date: "2021-06-19",
      end_date: "2021-12-18"
    },
    {
      id: "wdc003",
      name: "Java - Springboot",
      description:
        "So you two dig up, dig up dinosaurs? Remind me to thank John for a lovely weekend.",
      price: 10000,
      onOffer: true,
      start_date: "2021-07-20",
      end_date: "2021-06-19"
    },
  ];
  
  export default coursesData;
  