import React, { useContext } from 'react';
import { Redirect } from 'react-router';

import UserContext from '../UserContext';
import Courses from './Courses';

function Logout() {

    const { unsetUser, setUser} = useContext(UserContext);
    //clear the localstorage of the user's information
    unsetUser();

    //set the user state back to its original value
    setUser({email:null})

    //redirection to login page
    return(
        <Redirect to="/"  />
    )
}

export default Logout;