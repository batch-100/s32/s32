import React, { useEffect, useState } from "react";
import { Form, Button, Col } from "react-bootstrap";

function Register() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [isDisabled, setIsDisabled] = useState("true");

  //   useEffect(() => {
  //     console.log(email);
  //   }, [email]);
  //   useEffect(() => {
  //     console.log(password);
  //   }, [password]);
  //   useEffect(() => {
  //     console.log(passwordConfirm);
  //   }, [passwordConfirm]);
  function register(e) {
    //it prevents page redirection
    e.preventDefault();
    //it will clear the input fields

    setEmail("");
    setPassword("");
    setPasswordConfirm("");
    alert("Registration sucessful. You may now login.");
  }
  useEffect(() => {
    let isEmailNotEmpty = email !== "";
    let isPasswordNotEmpty = password !== "";
    let isPasswordConfirmNotEmpty = passwordConfirm !== "";
    let isPasswordMatched = password === passwordConfirm;

    //determine if all conditions have been met
    if (
      isEmailNotEmpty &&
      isPasswordNotEmpty &&
      isPasswordConfirmNotEmpty &&
      isPasswordMatched
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password, passwordConfirm]);

  //useEffect(()=>{
  //block of code goes here
  //it will trigger once lang if empty array
  //providing an empty array to a useEffect will trigge the effect once on initial render

  //}, [])
  //mount > render > re-render > unmount
  //mount - displays the elements in the app
  //render - reads the code and logic for the app
  //unmount - a component is unloaded form the app
  //rerender - nirerefresh niya lang ung app; updates the component with the updated logic/data

  return (
    <div>
      <h4>Register Here</h4>
      <hr />
      <Form onSubmit={register}>
        {
          <Form.Row>
            <Form.Group as={Col} controlId="userFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter First Name"
                required
              />
            </Form.Group>
            <Form.Group as={Col} controlId="userLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                required
              />
            </Form.Group>
          </Form.Row>
        }
        <Form.Row>
          <Form.Group as={Col} controlId="userEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              //bale object lang pala siya
              // event{
              //     target: {
              //         value:
              //     }
              // }
              //everytime na nagpapalit,
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We will never share your email address with anyone.
            </Form.Text>
          </Form.Group>

          <Form.Group as={Col}>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group as={Col}>
            <Form.Label>Re-enter Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={passwordConfirm}
              onChange={(e) => setPasswordConfirm(e.target.value)}
              required
            />
          </Form.Group>
        </Form.Row>

        <Button variant="primary" type="submit" disabled={isDisabled}>
          Register
        </Button>
      </Form>
    </div>
  );
}

export default Register;
