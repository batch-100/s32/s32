import React from 'react';
import Banner from '../components/Banner';

function Error() {
    const data ={
        title: "404 - Not Found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    return (
        
           <Banner data={data} />
        
    );
}

export default Error;