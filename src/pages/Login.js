import React, { useEffect, useState,useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Redirect } from "react-router";
import UserContext from '../UserContext';
import Courses from "./Courses";

function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isDisabled, setIsDisabled] = useState(false);

  function login(e) {
    e.preventDefault();
    setEmail("");
    setPassword("");
    alert(`${email} You are now logged in.`);
  };
  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  // useEffect(() => {
  //   let isEmailNotEmpty = email !== "";
  //   let isPasswordNotEmpty = password !== "";

  //   if(isEmailNotEmpty && isPasswordNotEmpty){
  //     setIsDisabled(false);
  //   } else{
  //     setIsDisabled(true);
  //   }

  // }, [email, password]);

  return (
    <div>
      <h4>Login</h4>
      <Form onSubmit={login}>
        <Form.Group>
          <Form.Label>Email address</Form.Label>
          <Form.Control 
          type="email" 
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)} />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group>
          <Form.Label>Password</Form.Label>
          <Form.Control 
          type="password" 
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>
        <Button variant="success" type="submit" disabled={!validateForm}>
          Login
        </Button>
      </Form>
    </div>
  );
}

export default Login;
