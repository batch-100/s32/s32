import React from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


function Home() {
    const data ={
        title: 'Zuitt Coding Bootcamp',
        content: 'Opportunities for everyone, everywhere',
        destination: "/courses",
        label: 'Enroll Now'
    }
    return (
        <div>
            <Banner data={data}/>
            <Container>
                <Highlights />
            </Container>
            
        </div>
    );
}

export default Home;