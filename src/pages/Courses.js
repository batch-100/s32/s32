import { Fragment } from 'react';

import Course from '../components/Course';
import coursesData from '../data/data'; //arayy

export default function Courses(){

    const CourseCards = coursesData.map((course)=>{ //userdefined ung CourseCards; ung courses daling lang sa data folder
        console.log(course);
        return (
            <Course key = {course.id} course={course} />
        );
    });
    return (
        <Fragment>
            {CourseCards}
        </Fragment>
    )
}
