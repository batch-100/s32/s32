import React, { Fragment, useState } from "react";
import { Container } from "react-bootstrap";
//{Components as Alias}
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import NavBar from "./components/NavBar"; //NavBar to kasi ung filename sa loob ng components folder

import Courses from "./pages/Courses";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";

import Logout from "./pages/Logout";
import Register from "./pages/Register";
import { UserProvider } from "./UserContext";

function App() {
  //state hook for the user state thats defined ehere  for  a global  scope
  //initialized as an object with properties from the localstaorage
  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    //data shared in localstorage is converted into string
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      email: null,
      isAdmin: null
    });
  };
  return (
    <Fragment>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <NavBar />
          <Container className="my-5">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/courses" component={Courses} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/logout" component={Logout} />
              <Route component={Error} />
            </Switch>
          </Container>
        </Router>
      </UserProvider>
    </Fragment>
  );
}

export default App;
