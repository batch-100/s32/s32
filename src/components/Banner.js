import React from "react";
import { Col, Jumbotron, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

function Banner({data}) {
  const {title, content, destination, label} = data;
  return (
    <div>
      <Row>
        <Col>
          <Jumbotron>
            <h1>{title}</h1>
            <p>{content}</p>
            <Link to={destination}>{label}</Link>
          </Jumbotron>
        </Col>
      </Row>
    </div>
  );
}

export default Banner;
