import React, { Fragment, useContext } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

function NavBar() {
  const { user } = useContext(UserContext);
  return (
    <Navbar br="light" expand="lg">
      <Navbar.Brand as={Link} to="/">
        React Bootstrap
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          <Nav.Link as={NavLink} to="/" exact>
            Home
          </Nav.Link>
          <Nav.Link as={NavLink} to="/courses" exact>
            Courses
          </Nav.Link>
          {user.email !== null ? (
            <Nav.Link as={NavLink} to="/logout" exact>
              Logout
            </Nav.Link>
          ) : (
            <Fragment>
              <Nav.Link as={NavLink} to="/login" exact>
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register" exact>
                Register
              </Nav.Link>
            </Fragment>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default NavBar;
