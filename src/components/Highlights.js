import React from "react";
import { Card, Col, Row } from "react-bootstrap";

function Highlights() {
  return (
    <div>
      <Row>
        <Col xs={12} md={4}>
          <Card className="card-highlight">
            <Card.Body>
              <Card.Title>
                <h2>Learn from Home</h2>
              </Card.Title>
              <Card.Text>
                Remind me to thank John for a lovely weekend. Yes, Yes, without
                the oops! Must go faster. Must go faster.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="card-highlight">
            <Card.Body>
              <Card.Title>
                <h2>Study Now, Pay Later</h2>
              </Card.Title>
              <Card.Text>
                Remind me to thank John for a lovely weekend. Yes, Yes, without
                the oops! Must go faster. Must go faster.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={4}>
          <Card className="card-highlight">
            <Card.Body>
              <Card.Title>
                <h2>Be Part of Our Community</h2>
              </Card.Title>
              <Card.Text>
                Remind me to thank John for a lovely weekend. Yes, Yes, without
                the oops! Must go faster. Must go faster.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default Highlights;
