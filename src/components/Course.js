import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Card, Button } from "react-bootstrap";

function Course({ course }) {
  const { name, description, price } = course;
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(10);
  const [isOpen, setIsOpen] = useState(true);

  useEffect(() => {
    if (seats === 0) {
      setIsOpen(false);
    }
  }, [seats]);

  function enroll() {
    setCount(count + 1);
    setSeats(seats - 1);
    console.log("Enrollees:" + count);
  }
  if (course) {
    return (
      <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>
            <span className="subtitle">Description:</span>
            {description}
            <br />
            <span className="subtitle">Price:</span>Php: {price}
            <br />
            <span>
              <h6>Enrollees: {count}</h6>
            </span>
            <span>
              <h6>Seats: {seats}</h6>
            </span>
          </Card.Text>
          <Card.Text>
            {isOpen ? (
              <Button variant="primary" onClick={enroll}>
                Enroll
              </Button>
            ) : (
              <Button variant="danger" disabled>
                Not Available
              </Button>
            )}
          </Card.Text>
        </Card.Body>
      </Card>
    );
  } else {
    return "";
  }
}

Course.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};

export default Course;
